#!/usr/bin/env python3

from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.compute import ComputeManagementClient
from azure.mgmt.subscription import SubscriptionClient
import os
import json
from pathlib import Path


client_id = os.getenv("CLIENT_ID")
secret = os.getenv("SECRET")
tenant = os.getenv("TENANT")
target_port = os.getenv("TARGET_PORT")

#target_port = "9182"

credentials = ServicePrincipalCredentials(
    client_id=client_id,
    secret=secret,
    tenant=tenant
)

def get_subscription_ids():
    sub_client = SubscriptionClient(credentials)
    subs = sub_client.subscriptions.list()
    subscriptions = []
    for sub in subs:
        if sub.state == "Enabled" and "desktop" not in sub.display_name \
        and "container" not in sub.display_name \
        and "Core Subscription" not in sub.display_name:
            subscriptions.append({"subscriptionID" : sub.subscription_id, "subscriptionName" : sub.display_name})
    return subscriptions

def process_azure_vms(subscriptions: list):
    
    for sub in subscriptions:
        client = ComputeManagementClient(credentials, sub["subscriptionID"])
        vms = client.virtual_machines.list_all()
        usage = client.usage.list("westeurope")
        for use in usage:
            print(f'name: {use.name.value} value: {use.current_value} limit: {use.limit}')
        for vm in vms:
            array_obj = []
            vm_obj = {"labels":{},"targets": []}
            if vm.storage_profile.os_disk.os_type == "Windows":
                vm_obj["labels"] = {"job": "amtmonitor", "subscription_id" : sub["subscriptionID"], "subscription_name" : sub["subscriptionName"], "vm_name": vm.name.lower()}
                vm_obj["targets"].append(vm.name+ ".we.azure.bkw.ch:"+target_port)
                array_obj.append(vm_obj)
                with open(f'./output/{vm.name}.json', "w", encoding="utf-8") as writeJSON:
                    # Create a json dump
                    json.dump((array_obj), writeJSON, indent=4, sort_keys=True)
                writeJSON.close()



for p in Path("./output").glob("*.json"):
    p.unlink()
all_subscriptions = get_subscription_ids()
process_azure_vms(all_subscriptions)